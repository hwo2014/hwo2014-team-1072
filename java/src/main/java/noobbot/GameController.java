package noobbot;

import java.lang.reflect.Type;
import java.util.List;

import noobbot.vo.GamePosition;
import noobbot.vo.Id;
import noobbot.vo.Piece;
import noobbot.vo.Track;

import java.io.PrintWriter;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class GameController {

	private Gson gson = new Gson();
	private Track track;


	public Piece getCurrentPiece() {
		return currentPiece;
	}


	private Id id;
	private Piece currentPiece;
	private Integer currentPieceIndex;
	private GamePosition currentCarPosition;
	private float incurve_nextcurve;

	public float getIncurve_nextcurve() {
		return incurve_nextcurve;
	}


	public void setIncurve_nextcurve(float incurve_nextcurve) {
		this.incurve_nextcurve = incurve_nextcurve;
	}


	public float getIncurve_nextnotcurve() {
		return incurve_nextnotcurve;
	}


	public void setIncurve_nextnotcurve(float incurve_nextnotcurve) {
		this.incurve_nextnotcurve = incurve_nextnotcurve;
	}


	public float getInstraight_nextcurve_prevstraight() {
		return instraight_nextcurve_prevstraight;
	}


	public void setInstraight_nextcurve_prevstraight(
			float instraight_nextcurve_prevstraight) {
		this.instraight_nextcurve_prevstraight = instraight_nextcurve_prevstraight;
	}


	public float getInstraight_nextcurve() {
		return instraight_nextcurve;
	}


	public void setInstraight_nextcurve(float instraight_nextcurve) {
		this.instraight_nextcurve = instraight_nextcurve;
	}


	public float getInstraight_nextstraight() {
		return instraight_nextstraight;
	}


	public void setInstraight_nextstraight(float instraight_nextstraight) {
		this.instraight_nextstraight = instraight_nextstraight;
	}


	private float incurve_nextnotcurve;
	private float instraight_nextcurve_prevstraight;
	private float instraight_nextcurve;
	private float instraight_nextstraight;
	private int timeTick = -1;
	private double distanceTravelled = 0.0;
	private double acceleration;
	private double velocity;
	private boolean firstCurveFound = false;
	private boolean firstCurveSeen = false;
	private boolean firstCurveover = false;
	private Double firstSlipangle;
	private Double secondSlipangle;
	private Double firstSlipdistance;
	private Double secondSlipDistance;
	public Integer getCurrentPieceIndex() {
		return currentPieceIndex;
	}


	public Id getId() {
		return id;
	}


	public Track getTrack() {
		return track;
	}


	public List<GamePosition> getCurrentGamePosition() {
		return currentGamePosition;
	}


	private List<GamePosition> currentGamePosition;

	public void init(String initString) {
		track = gson.fromJson(initString, Track.class);
		//System.out.println(track);
		Piece previousPiece = null;
		for(Piece piece : track.getRace().getTrack().getPieces()) {
			piece.setIsPieceBeforeSwitch(false);
			if(piece.getSwitch() != null) {
				piece.setPieceType(Piece.PieceType.SWITCH);
				if(previousPiece != null)
					previousPiece.setIsPieceBeforeSwitch(true);
			}
			else if(piece.getLength() != null)
				piece.setPieceType(Piece.PieceType.STRAIGHT);
			else
				piece.setPieceType(Piece.PieceType.CURVE);
			previousPiece = piece;
		}
		//System.out.println(track);
	}

	public SendMsg getNextCommand() {
		//Main.writer2.println("time tick" + timeTick);
		timeTick++;
		Switch possibleSwitch = null;
		if(firstCurveover)
			return findAcceleration();
		else
			return findAccelerationInitial();
		//if(currentPiece.getIsPieceBeforeSwitch())
			//possibleSwitch = getIfSwitch();
		//getIfSwitch();
		//if(possibleSwitch == null)
			
		//else {
			//System.out.println("switch: " + possibleSwitch.getValue());
			//return possibleSwitch;
		}
	private Throttle findAccelerationInitial()
	{
		Piece nextPiece = track.getRace().getTrack().getPieces().get(currentPieceIndex+1);
		if(currentPiece.getAngle() != null)
		{
			if(!firstCurveSeen && Math.abs(currentPiece.getAngle()) > 0)
			{
				firstCurveSeen = true;
				firstSlipangle = Math.abs(currentPiece.getAngle());
				firstSlipdistance = (Math.abs(currentPiece.getAngle())/360 ) * 2 * Math.PI * currentPiece.getRadius();
				return new Throttle(0.0);
			}
			if(firstCurveSeen)
			{
				secondSlipangle = Math.abs(currentPiece.getAngle());
				secondSlipDistance = (Math.abs(currentPiece.getAngle())/360 ) * 2 * Math.PI * currentPiece.getRadius();
				firstCurveover = true;
				return new Throttle(0.0);
			}
		}
	if(nextPiece.getPieceType().equals(Piece.PieceType.CURVE) && nextPiece.getAngle() != null)
	{
		if(!firstCurveFound)
			firstCurveFound = true;
		return new Throttle(0.0);
	}
	else 
	{
		return new Throttle(1.0);
	}
	}
		/*Switch possibleSwitch = null;
		if(currentPiece.getIsPieceBeforeSwitch())
			possibleSwitch = getIfSwitch();
		//getIfSwitch();
		if(possibleSwitch == null)
			return getThrottle();
		else {
			System.out.println("switch: " + possibleSwitch.getValue());
			return possibleSwitch;
		}*/
	//}
	//germany
	/*	private Throttle findAcceleration()
	{
		if(currentPieceIndex < track.getRace().getTrack().getPieces().size() - 1)
		{
			if(track.getRace().getTrack().getPieces().get(currentPieceIndex).getPieceType().equals(Piece.PieceType.CURVE))
			{
				//System.out.println("incurve");
				return new Throttle(0.4);
			}
			else if(track.getRace().getTrack().getPieces().get(currentPieceIndex+1).getPieceType().equals(Piece.PieceType.CURVE))
			{

			timeTick++;
				//System.out.println("nextcurve");
				if(currentPieceIndex > 1 && !track.getRace().getTrack().getPieces().get(currentPieceIndex-2).getPieceType().equals(Piece.PieceType.CURVE))
					return new Throttle(0.2);
				else if(currentPieceIndex > 0 && !track.getRace().getTrack().getPieces().get(currentPieceIndex-1).getPieceType().equals(Piece.PieceType.CURVE))
					return new Throttle(0.4);	
				else
					return new Throttle(0.4);	
			}
			else
			{
				//System.out.println("instraight");
				return new Throttle(0.7);
			}
		}
		else
		{
			System.out.println("lap over");
			return new Throttle(0.8);
		}
	}*/

	//u.s.a
	/*private Throttle findAcceleration()
	{
		if(currentPieceIndex < track.getRace().getTrack().getPieces().size() - 1)
		{
			//if you are in curve
			if(track.getRace().getTrack().getPieces().get(currentPieceIndex).getPieceType().equals(Piece.PieceType.CURVE))
			{
				//System.out.println("incurve");
				return new Throttle(0.90);

			}
			//if your next is curve
			else if(track.getRace().getTrack().getPieces().get(currentPieceIndex+1).getPieceType().equals(Piece.PieceType.CURVE))
			{
				//System.out.println("nextcurve");
				// if you travelling 2 straight line
				if(currentPieceIndex > 1 && !track.getRace().getTrack().getPieces().get(currentPieceIndex-2).getPieceType().equals(Piece.PieceType.CURVE))
					return new Throttle(0.77);
				//if you are travelling one straight line
				else if(currentPieceIndex > 0 && !track.getRace().getTrack().getPieces().get(currentPieceIndex-1).getPieceType().equals(Piece.PieceType.CURVE))
					return new Throttle(0.78);
				//you are travelling in a curve
				else
					return new Throttle(0.9);	
			}
			else
			{
				//System.out.println("instraight");
				return new Throttle(1.0);
			}
		}
		else
		{
			System.out.println("lap over");
			return new Throttle(1.0);
		}
	}*/

	// learning
	/*private Throttle findAcceleration()
	{
		if(currentPieceIndex < track.getRace().getTrack().getPieces().size())
		{
			//if you are in curve
			if(track.getRace().getTrack().getPieces().get(currentPieceIndex).getPieceType().equals(Piece.PieceType.CURVE))
			{
				//System.out.println("incurve");
				if(track.getRace().getTrack().getPieces().get(currentPieceIndex+1).getPieceType().equals(Piece.PieceType.CURVE))
				{
					return new Throttle(incurve_nextcurve);
				}
				else
				{
					return new Throttle(incurve_nextnotcurve);	
				}

			}
			//if you are not in curve but next is curve
			else if(track.getRace().getTrack().getPieces().get(currentPieceIndex+1).getPieceType().equals(Piece.PieceType.CURVE))
			{
				//System.out.println("nextcurve");
				// if you travelling 2 straight line
				if(currentPieceIndex > 0 && !track.getRace().getTrack().getPieces().get(currentPieceIndex-1).getPieceType().equals(Piece.PieceType.CURVE))
					return new Throttle(instraight_nextcurve_prevstraight);
				else
					return new Throttle(instraight_nextcurve);	
			}
			else
			{
				//System.out.println("instraight");
				return new Throttle(instraight_nextstraight);
			}
		}
		else
		{
			System.out.println("lap over");
			return new Throttle(1.0);
		}
	}*/

	public Throttle getNextCommand1() {
		// TODO Auto-generated method stub
		return new Throttle(1.0);
	}

	private Throttle findAcceleration()
	{
		// if we start with a curve ????????
		// if the next is curve from the start index ?????????
		
		int currentPieceIndex = currentCarPosition.getPiecePosition().getPieceIndex();
		Double currentPieceLength = track.getRace().getTrack().getPieces().get(currentPieceIndex).getLength();
		int totalPieces = track.getRace().getTrack().getPieces().size();
		Double currentInpieceDistance = currentCarPosition.getPiecePosition().getInPieceDistance();
		
		
		// calculate the acceleration
		if(timeTick == 1)
		{
			acceleration = 2 * currentInpieceDistance;

		}
		// calculate the distance to the next curve
		boolean untillCurve = true;
		for(int i =currentPieceIndex%totalPieces; i < totalPieces && untillCurve; i++)
		{
			Double distancetoNextcurve = currentPieceLength -currentInpieceDistance ;
			boolean isCurve = track.getRace().getTrack().getPieces().get(i).getPieceType().equals(Piece.PieceType.CURVE);
			boolean isCurveSwitch = track.getRace().getTrack().getPieces().get(i).getAngle() != null;
			if(!isCurve && isCurveSwitch)
			{
				distancetoNextcurve += track.getRace().getTrack().getPieces().get(i).getLength();
			}
		}
		
		// initial deceleration if it is a curve 
	
		// calculate the rate of change
		
		//Main.writer2.println("distance travelled" + distanceTravelled);
		//float tempDistance = 0;
		//System.out.println("current index"+currentCarPosition.getPiecePosition().getPieceIndex());
		//System.out.println("size"+track.getRace().getTrack().getPieces().size());
		/*for(int i=0;i<(currentCarPosition.getPiecePosition().getPieceIndex()%track.getRace().getTrack().getPieces().size());i++)
		{
			//System.out.println("inside loop" + i);
			if(track.getRace().getTrack().getPieces().get(i).getPieceType().equals(Piece.PieceType.CURVE) || track.getRace().getTrack().getPieces().get(i).getAngle() != null)
			{
				tempDistance +=  (Math.abs(track.getRace().getTrack().getPieces().get(i).getAngle())/360 ) * 2 * Math.PI * track.getRace().getTrack().getPieces().get(i).getRadius();
			}
			else
			tempDistance += track.getRace().getTrack().getPieces().get(i).getLength();
		}
		distanceTravelled = tempDistance + currentCarPosition.getPiecePosition().getInPieceDistance();
		Main.writer2.println("velocity is " + distanceTravelled/timeTick);*/
		boolean curve = track.getRace().getTrack().getPieces().get(currentPieceIndex).getPieceType().equals(Piece.PieceType.CURVE);
		if(currentPieceIndex < track.getRace().getTrack().getPieces().size() && curve)
		{		
			if(Math.abs(currentCarPosition.getAngle()) > 25 && Math.abs(currentCarPosition.getAngle())  < 35)
			{
				Main.writer2.println(currentCarPosition.getAngle());
				return new Throttle(0.0);
			}
			if(Math.abs(currentCarPosition.getAngle()) > 35 && Math.abs(currentCarPosition.getAngle())  < 45)
			{
				Main.writer2.println(currentCarPosition.getAngle());
				return new Throttle(0.0);
			}
			else if(Math.abs(currentCarPosition.getAngle()) > 45)
			{
				Main.writer2.println(currentCarPosition.getAngle());

				return new Throttle(0.0);
			}
			else
				return new Throttle(0.8);

		}
		else
			return new Throttle(0.8);
	}

	/*
	private Throttle findAcceleration()
	{
		boolean curve = track.getRace().getTrack().getPieces().get(currentPieceIndex).getPieceType().equals(Piece.PieceType.CURVE);
		if(currentPieceIndex < track.getRace().getTrack().getPieces().size() && curve)
		{
		return new Throttle(0.6);
		}
		else
		return new Throttle(1.0);
		}*/
	private Switch getIfSwitch() {

		if(currentCarPosition.getPiecePosition().getLane().getEndLaneIndex() != currentCarPosition.getPiecePosition().getLane().getEndLaneIndex())
			return null;


		if(currentPieceIndex < track.getRace().getTrack().getPieces().size() - 1) {
			Piece nextPiece = track.getRace().getTrack().getPieces().get(currentPieceIndex + 1);
			Double pieceLength = currentPiece.getLength();
			if(pieceLength == null)
				pieceLength = (Math.abs(currentPiece.getAngle())/360 ) * 2 * Math.PI * currentPiece.getRadius();
			double percentTravelled = (currentCarPosition.getPiecePosition().getInPieceDistance() / pieceLength)*100;
			if(nextPiece.getPieceType().equals(Piece.PieceType.SWITCH) && percentTravelled > 90) {
				int index = currentPieceIndex + 1;
				Piece nextCurve = null;
				while(index < track.getRace().getTrack().getPieces().size() - 1) {
					if(track.getRace().getTrack().getPieces().get(index).getPieceType().equals(Piece.PieceType.CURVE)) {
						nextCurve = track.getRace().getTrack().getPieces().get(index);
						break;
					}
					index++;
				}
				if(nextCurve != null) {
					if(nextCurve.getAngle() < 0)
					{
						if(currentCarPosition.getPiecePosition().getLane().getStartLaneIndex() < 0)
							return null;
						else
							return new Switch("Left"); 
					}
					else if(nextCurve.getAngle() > 0)	
					{
						//if(currentCarPosition.getPiecePosition().getLane().getStartLaneIndex() == this.track.getRace().getTrack().getLanes().size()-1)
						if(currentCarPosition.getPiecePosition().getLane().getStartLaneIndex() > 0)
							return null;
						else
							return new Switch("Right");
					}
				}
			}
		}
		return null;
	}


	private Throttle getThrottle() {
		//return new Throttle(0.62);
		System.out.println(currentPieceIndex);
		Piece nextPiece = null;
		if(currentPieceIndex < track.getRace().getTrack().getPieces().size() - 1)
			nextPiece = track.getRace().getTrack().getPieces().get(currentPieceIndex + 1);
		if(currentPiece.getPieceType().equals(Piece.PieceType.CURVE)) {
			return new Throttle(estimateThrottleAtCurve());
		}
		else if(nextPiece != null && nextPiece.getPieceType().equals(Piece.PieceType.CURVE))
			return new Throttle(estimateThrottleForPieceBeforeCurve(nextPiece));
		else
			return new Throttle(estimateThrottle());


	}

	private Double estimateThrottleForPieceBeforeCurve(Piece nextPiece) {
		Double pieceLength = currentPiece.getLength();
		if(pieceLength == null)
			pieceLength = (Math.abs(currentPiece.getAngle())/360 ) * 2 * Math.PI * currentPiece.getRadius();
		double percentTravelled = (currentCarPosition.getPiecePosition().getInPieceDistance() / pieceLength)*100;
		if(percentTravelled > 60)
			return (45 / Math.abs(nextPiece.getAngle())) * 0.4;
		else
			return estimateThrottle();
	}

	private Double estimateThrottle() {
		if(currentCarPosition.getPiecePosition().getLap() != 0) {
			//return 0.8;
			return (currentPiece.getMaxDistancePerTick()/currentDistanceChangeWithinPiece)*1.0;
		}
		else
			return 0.9;
	}

	private Double previousAngle = 0.0;
	private Double estimateThrottleAtCurve() {
		Double pieceLength = currentPiece.getLength();
		if(pieceLength == null)
			pieceLength = (Math.abs(currentPiece.getAngle())/360 ) * 2 * Math.PI * currentPiece.getRadius();
		double percentTravelled = (currentCarPosition.getPiecePosition().getInPieceDistance() / pieceLength)*100;
		if(percentTravelled > 80 && !(track.getRace().getTrack().getPieces().get(currentPieceIndex + 1).getPieceType().equals(Piece.PieceType.CURVE)))
		{
			return 0.8;
		}

		double changeInAngle = (( Math.abs(currentCarPosition.getAngle()) - previousAngle) / Math.abs(currentPiece.getAngle()))*100;
		double changeInDistance = (currentDistanceChangeWithinPiece / ((Math.abs(currentPiece.getAngle())/360 ) * 2 * Math.PI * currentPiece.getRadius()))*100;
		previousAngle = currentCarPosition.getAngle();
		if(changeInAngle > changeInDistance)
			return 0.25;
		else
			return (40 / Math.abs(currentPiece.getAngle())) * 0.4;

	}

	public void setGamePosition(String gamePositionString) {
		Type listType = new TypeToken<List<GamePosition>>() {}.getType();
		List<GamePosition> gamePos = gson.fromJson(gamePositionString, listType);
		currentGamePosition = gamePos;
		calCurrentPiece();
		calMaxDistancePerTickWithinPiece();
	}

	private double prevoiusPositionWithinPiece = 0;
	private double currentDistanceChangeWithinPiece = 0;
	private void calMaxDistancePerTickWithinPiece() {
		currentDistanceChangeWithinPiece = currentCarPosition.getPiecePosition().getInPieceDistance() - prevoiusPositionWithinPiece;
		if(currentDistanceChangeWithinPiece > currentPiece.getMaxDistancePerTick() && currentCarPosition.getPiecePosition().getLap() == 0)
			currentPiece.setMaxDistancePerTick(currentDistanceChangeWithinPiece);
		prevoiusPositionWithinPiece = currentCarPosition.getPiecePosition().getInPieceDistance();
	}

	private void calCurrentPiece() {
		for(GamePosition gamePos : currentGamePosition) {
			if(gamePos.getId().equals(id)) {
				currentCarPosition = gamePos;
				if(currentPieceIndex != gamePos.getPiecePosition().getPieceIndex()) {
					prevoiusPositionWithinPiece = 0;
					//previousAngle = 0.0;
				}
				currentPieceIndex = gamePos.getPiecePosition().getPieceIndex();
				currentPiece = track.getRace().getTrack().getPieces().get(gamePos.getPiecePosition().getPieceIndex());
			}
		}
	}


	public void setId(String data) {

		id = gson.fromJson(data, Id.class);
	}




}
