package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

//import javax.swing.text.MaskFormatter;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class Main {
	public static void main(String... args) throws IOException {
		String host = args[0];
		int port = Integer.parseInt(args[1]);
		String botName = args[2];
		String botKey = args[3];

		System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		final Socket socket = new Socket(host, port);
		final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

		final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));
		CreateRace createrace = new CreateRace();
		BotId botId =  new BotId();
		botId.setName(botName);
		botId.setKey(botKey);
		createrace.setBotId(botId);
		createrace.setTrackName("keimola");
		createrace.setCarCount(1);
		createrace.setMsgType("createRace");
		new Main(reader, writer, createrace);
		//new Main(reader, writer, new Join(botName, botKey));

	}

	final Gson gson = new Gson();
	private PrintWriter writer;
	public static PrintWriter writer2 ;

	public Main(final BufferedReader reader, final PrintWriter writer, final CreateRace createRace) throws IOException {
	//public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
		this.writer = writer;
		String line = null;
		//Gson gson1 = new GsonBuilder().setPrettyPrinting().create();
		/*String json1 = gson.toJson(join);
		System.out.println(json1);
		send(join);*/
		//String json1 = gson.toJson(createRace);
		//System.out.println(json1);
		send(createRace);
		PrintWriter writer1 = new PrintWriter("test_data_8.txt", "UTF-8");
		writer2 = new PrintWriter("test_data_9.txt", "UTF-8");
		//writer1.append("[");
		GameController gameController = new GameController();
		
		while((line = reader.readLine()) != null) {
			final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			String json = gson.toJson(msgFromServer.data);
			writer2.println(json);
			try{
				if (msgFromServer.msgType.equals("carPositions")) {
					//send(new Throttle(0.62));
					
					gameController.setGamePosition(msgFromServer.data.toString());
					send(gameController.getNextCommand());
				} else if (msgFromServer.msgType.equals("join")) {
					System.out.println("Joined");
				} else if (msgFromServer.msgType.equals("gameInit")) {
					//Gson gson = new GsonBuilder().setPrettyPrinting().create();
					//String json = gson.toJson(msgFromServer.data);
					//writer1.println(json + ",");
					/*Gson gson = new GsonBuilder().setPrettyPrinting().create();
				String json = gson.toJson(msgFromServer.data);
				writer1.println(json);*/
					//System.out.println(json);
					//System.out.println(msgFromServer.data.toString());
					gameController.init(msgFromServer.data.toString());
					System.out.println("Race init");
				} else if (msgFromServer.msgType.equals("gameEnd")) {
					System.out.println("Race end");
				} else if (msgFromServer.msgType.equals("gameStart")) {
					System.out.println("Race start");
				} 
				else if(msgFromServer.msgType.equals("dnf"))
				{
					System.out.println("Disqualified");
					/*Gson gson = new GsonBuilder().setPrettyPrinting().create();
					String json = gson.toJson(msgFromServer.data);
					System.out.println(json);
					writer1.println(json);*/
					System.out.println(msgFromServer.data);
				} else if(msgFromServer.msgType.equals("yourCar")) {
					gameController.setId(msgFromServer.data.toString());
				}
				else if(msgFromServer.msgType.equals("spawn"))
				{
					writer2.println("spawn" + ",");
					//Gson gson = new GsonBuilder().setPrettyPrinting().create();
					//String json = gson.toJson(msgFromServer.data);
					//writer1.println(json + ",");
				}
				else if(msgFromServer.msgType.equals("crash")) {
					System.out.println("crash");
					writer2.println("crash" + ",");
					//Gson gson = new GsonBuilder().setPrettyPrinting().create();
					//String json = gson.toJson(msgFromServer.data);
					//writer1.println(json + ",");
					/*Gson gson = new GsonBuilder().setPrettyPrinting().create();
					String json = gson.toJson(msgFromServer.data);
					System.out.println(json);*/
				}
				else {

					send(new Ping());
				}
			}

			catch(Exception ex)
			{
				System.out.println(ex.getMessage());
				ex.printStackTrace();
				/*Gson gson = new GsonBuilder().setPrettyPrinting().create();
				String json = gson.toJson(msgFromServer.data);*/
				System.out.println(msgFromServer.data);
				send(gameController.getNextCommand1());
			}
		}
		//writer1.append("]");
		writer2.close();
	}

	private void send(final SendMsg msg) {
		writer.println(msg.toJson());
		writer.flush();
	}
}

abstract class SendMsg {
	public String toJson() {
		return new Gson().toJson(new MsgWrapper(this));
	}

	protected Object msgData() {
		return this;
	}

	protected abstract String msgType();
}

class MsgWrapper {
	public final String msgType;
	public final Object data;

	MsgWrapper(final String msgType, final Object data) {
		this.msgType = msgType;
		this.data = data;
	}

	public MsgWrapper(final SendMsg sendMsg) {
		this(sendMsg.msgType(), sendMsg.msgData());
	}
}

class Join extends SendMsg {
	public final String name;
	public final String key;

	Join(final String name, final String key) {
		this.name = name;
		this.key = key;
	}

	@Override
	protected String msgType() {
		return "join";
	}
}
class JoinRace extends SendMsg {
	public final String name;
	public final String key;
	public final String trackName;

	JoinRace(final String name, final String key) {
		this.name = name;
		this.key = key;
		this.trackName = "Germany";
	}

	@Override
	protected String msgType() {
		return "joinRace";
	}
}



class CreateRace extends SendMsg{


	private String msgType;
	private BotId botId;
	private String trackName;
	private Integer carCount;

	public BotId getBotId() {
		return botId;
	}

	public void setBotId(BotId botId) {
		this.botId = botId;
	}

	public String getTrackName() {
		return trackName;
	}

	public void setTrackName(String trackName) {
		this.trackName = trackName;
	}
	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	@Override
	protected String msgType() {
		// TODO Auto-generated method stub
		return "createRace";
	}

	public Integer getCarCount() {
		return carCount;
	}

	public void setCarCount(Integer carCount) {
		this.carCount = carCount;
	}
}
class BotId {

	private String name;
	private String key;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}


class Ping extends SendMsg {
	@Override
	protected String msgType() {
		return "ping";
	}
}

class Throttle extends SendMsg {
	private double value;

	public Throttle(double value) {
		this.value = value;
	}

	@Override
	protected Object msgData() {
		return value;
	}

	@Override
	protected String msgType() {
		return "throttle";
	}

	public double getThrottleVal() {
		return value;
	}
}

class Switch extends SendMsg {

	private String value;

	public Switch(String direction) {
		this.value = direction;
	}

	@Override
	protected String msgType() {

		return "switchLane";
	}

	@Override
	protected Object msgData() {
		return value;
	}

	public String getValue() {
		return value;
	}

}