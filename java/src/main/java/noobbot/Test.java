package noobbot;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import noobbot.vo.GamePosition;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class Test {

	public static void main(String[] args) throws IOException {
		String trackString = new String(Files.readAllBytes(Paths.get("/home/rajneesh/javaWS/temp/test_data_1.txt")));
		GameController gc = new GameController();
		gc.setId("{\"name\": \"Changers\",\"color\": \"red\"}");
		gc.init(trackString);
		
		String gamePositionsString = new String(Files.readAllBytes(Paths.get("/home/rajneesh/javaWS/temp/test_data_2.txt")));
		Type listType = new TypeToken<List<List<GamePosition>>>() {}.getType();
		Gson gson = new Gson();
		List<List<GamePosition>> glist = gson.fromJson(gamePositionsString, listType);
		for(List<GamePosition> g : glist) {
			System.out.println();
			gc.setGamePosition(gson.toJson(g));
 			System.out.print(gc.getCurrentPieceIndex() + " " + gc.getCurrentPiece().getPieceType() + " " + gc.getCurrentPiece().getMaxDistancePerTick() + " " + ((SendMsg)gc.getNextCommand()).msgData());
		}
	}

}
