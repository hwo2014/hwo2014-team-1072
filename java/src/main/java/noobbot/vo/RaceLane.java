
package noobbot.vo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class RaceLane {

    @Expose
    private Integer startLaneIndex;
    @Expose
    private Integer endLaneIndex;

    public Integer getStartLaneIndex() {
        return startLaneIndex;
    }

    public void setStartLaneIndex(Integer startLaneIndex) {
        this.startLaneIndex = startLaneIndex;
    }

    public Integer getEndLaneIndex() {
        return endLaneIndex;
    }

    public void setEndLaneIndex(Integer endLaneIndex) {
        this.endLaneIndex = endLaneIndex;
    }

}
