
package noobbot.vo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class RaceSession {

    @Expose
    private Double laps;
    @Expose
    private Double maxLapTimeMs;
    @Expose
    private Boolean quickRace;

    public Double getLaps() {
        return laps;
    }

    public void setLaps(Double laps) {
        this.laps = laps;
    }

    public Double getMaxLapTimeMs() {
        return maxLapTimeMs;
    }

    public void setMaxLapTimeMs(Double maxLapTimeMs) {
        this.maxLapTimeMs = maxLapTimeMs;
    }

    public Boolean getQuickRace() {
        return quickRace;
    }

    public void setQuickRace(Boolean quickRace) {
        this.quickRace = quickRace;
    }

}
