
package noobbot.vo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class PiecePosition {

    @Expose
    private Integer pieceIndex;
    @Expose
    private Double inPieceDistance;
    @Expose
    private RaceLane lane;
    @Expose
    private Integer lap;

    public Integer getPieceIndex() {
        return pieceIndex;
    }

    public void setPieceIndex(Integer pieceIndex) {
        this.pieceIndex = pieceIndex;
    }

    public Double getInPieceDistance() {
        return inPieceDistance;
    }

    public void setInPieceDistance(Double inPieceDistance) {
        this.inPieceDistance = inPieceDistance;
    }

    public RaceLane getLane() {
        return lane;
    }

    public void setLane(RaceLane lane) {
        this.lane = lane;
    }

    public Integer getLap() {
        return lap;
    }

    public void setLap(Integer lap) {
        this.lap = lap;
    }

}
