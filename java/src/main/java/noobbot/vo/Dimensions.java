
package noobbot.vo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Dimensions {

    @Expose
    private Double length;
    @Expose
    private Double width;
    @Expose
    private Double guideFlagPosition;

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getGuideFlagPosition() {
        return guideFlagPosition;
    }

    public void setGuideFlagPosition(Double guideFlagPosition) {
        this.guideFlagPosition = guideFlagPosition;
    }

}
