
package noobbot.vo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Lane {

    @Expose
    private Double distanceFromCenter;
    @Expose
    private Double index;

    public Double getDistanceFromCenter() {
        return distanceFromCenter;
    }

    public void setDistanceFromCenter(Double distanceFromCenter) {
        this.distanceFromCenter = distanceFromCenter;
    }

    public Double getIndex() {
        return index;
    }

    public void setIndex(Double index) {
        this.index = index;
    }

}
