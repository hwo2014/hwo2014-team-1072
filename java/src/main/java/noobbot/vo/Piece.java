
package noobbot.vo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Piece {
	
	public enum PieceType {STRAIGHT,SWITCH,CURVE};
	
	private PieceType pieceType;

    public PieceType getPieceType() {
		return pieceType;
	}

	public void setPieceType(PieceType pieceType) {
		this.pieceType = pieceType;
	}

	@Expose
    private Double length;
    @SerializedName("switch")
    @Expose
    private Boolean _switch;
    @Expose
    private Double radius;
    @Expose
    private Double angle;

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Boolean getSwitch() {
        return _switch;
    }

    public void setSwitch(Boolean _switch) {
        this._switch = _switch;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Double getAngle() {
        return angle;
    }

    public void setAngle(Double angle) {
        this.angle = angle;
    }
    
    private Double maxDistancePerTick = 0.0; 

	public Double getMaxDistancePerTick() {
		return maxDistancePerTick;
	}

	public void setMaxDistancePerTick(Double maxDistancePerTick) {
		this.maxDistancePerTick = maxDistancePerTick;
	}
	
	private Boolean isPieceBeforeSwitch;

	public Boolean getIsPieceBeforeSwitch() {
		return isPieceBeforeSwitch;
	}

	public void setIsPieceBeforeSwitch(Boolean isPieceBeforeSwitch) {
		this.isPieceBeforeSwitch = isPieceBeforeSwitch;
	}
    
    

}
