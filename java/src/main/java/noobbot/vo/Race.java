
package noobbot.vo;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Race {

    @Expose
    private Track_ track;
    @Expose
    private List<Car> cars = new ArrayList<Car>();
    @Expose
    private RaceSession raceSession;

    public Track_ getTrack() {
        return track;
    }

    public void setTrack(Track_ track) {
        this.track = track;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

    public RaceSession getRaceSession() {
        return raceSession;
    }

    public void setRaceSession(RaceSession raceSession) {
        this.raceSession = raceSession;
    }

}
