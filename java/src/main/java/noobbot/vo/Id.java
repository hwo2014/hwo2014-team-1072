
package noobbot.vo;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;

@Generated("org.jsonschema2pojo")
public class Id {

    @Expose
    private String name;
    @Expose
    private String color;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    @Override
    public boolean equals(Object id) {
    	if(id instanceof Id) {
    		if(((Id) id).getColor().equals(color) && ((Id)id).getName().equals(name))
    			return true;
    		else
    			return false;
    	}
    	else
    		return false;
    		
    }

}
